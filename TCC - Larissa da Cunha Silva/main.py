from pdf2image import convert_from_path, convert_from_bytes

images = convert_from_path('/home/larissa/Área de Trabalho/PDF TCC/merge/pedido_insercao_87795_2020_08_27_11_05_45-mesclado.pdf')

for i, image in enumerate(images):
    fname = "image" + str(i) + ".png"
    image.save(fname, "PNG")