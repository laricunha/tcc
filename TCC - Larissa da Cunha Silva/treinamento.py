from keras.models import Sequential, load_model
from keras.layers import Conv2D, MaxPooling2D, Flatten
from keras.layers import Dense
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from numpy import expand_dims, argmax
from PIL import Image
import pytesseract as ocr
import regex as re

imgW, imgH = 400, 400
kernel, downscale = 3,2
path = 'images'

trainImgs = ImageDataGenerator(
    rescale=1./255, shear_range=0.2,
    zoom_range=0.2, horizontal_flip=True)
testImgs = ImageDataGenerator(rescale=1./255)

train = trainImgs.flow_from_directory(
    path + '/train', target_size=(imgW, imgH),
    batch_size=16, class_mode='categorical'
)

test = testImgs.flow_from_directory(
    path + '/validate', target_size=(imgW, imgH),
    batch_size=16, class_mode='categorical'
)

classes = list(train.class_indices.keys())

cnn = Sequential()
cnn.add(Conv2D(
    32, kernel, input_shape=(imgW, imgH, 3), activation='relu'
))

cnn.add(MaxPooling2D(
    pool_size=(downscale, downscale)
))

cnn.add(Conv2D(
    64, kernel, activation='relu'
))

cnn.add(MaxPooling2D(
    pool_size=(downscale, downscale)
))

cnn.add(Flatten())

cnn.add(Dense(
    units=121, activation='relu'
))

cnn.add(Dense(
    units=len(classes), activation='softmax'
))

cnn.compile(
    optimizer='adam',
    loss='categorical_crossentropy',
    metrics=['accuracy']
)

cnn.fit_generator(
    train,
    steps_per_epoch=8,
    epochs=50,
    validation_data=test,
    validation_steps=8
)

cnn.save('cnn-mapa.h5')

cnn = load_model('cnn-mapa.h5')

for i in range(90,110):
    file = path + '/validate' + '/validação' +'/image' + str(i) + '.png'
    img = image.load_img(
        file, target_size=(imgW, imgH)
    )
    y = cnn.predict(expand_dims(
        image.img_to_array(img), axis=0
    ))
    print('Mapa in:', file, ':', classes[argmax(y)])

    if classes[argmax(y)] == 'válido':
        open1 = Image.open(file)
        dados = ocr.image_to_string(open1, lang='por')

        cliente = re.findall('cliente.*', dados)

        qtd = re.findall('Qtd.*', dados)

        tabela = re.findall('Total Tabela.*', dados)

        desc = re.findall('Desc.(%).*', dados)

        desc_med = re.findall('Desc. Médio.*', dados)

        valor_bru = re.findall('Valor Bruto.*', dados)

        com = re.findall('Com.(%).*', dados)

        valor_liq = re.findall('Valor Líquido.*', dados)

        print(str(cliente), '\n', str(qtd), '\n', str(tabela), '\n', str(desc), '\n',
              str(desc_med), '\n', str(valor_bru), '\n', str(com), '\n', str(valor_liq))